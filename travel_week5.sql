create database travel_week5;

use travel_week5;
create table PASSENGER(Passenger_name varchar(20),Category varchar(20),Gender varchar(20),Boarding_City varchar(20),Destination_City varchar(20),Distance int,Bus_Type varchar(20));
create table PRICE(Bus_Type varchar(20),Distance int,Price int);

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');
select * from passenger;

insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);
select * from price;

SELECT COUNT(CASE WHEN Gender='M' THEN 1  END) As Male, COUNT(CASE WHEN Gender='F' THEN 1  END) As Female, COUNT(*) as Total_persons_who_travelled_minimum_of_600KMs from passenger where Distance>=600 GROUP BY Gender;

Select min(Price) as Minimum_ticket_Price_for_sleeperBus from price where Bus_Type="Sleeper";

Select Passenger_name from passenger where Passenger_name LIKE "S%";

Select p.passenger_name, p.Boarding_city, p.Destination_city, p.Bus_type, pi.price from passenger p inner join price pi on p.distance=pi.distance and p.Bus_type=pi.Bus_type group by p.passenger_name;

Select p.passenger_name, pi.price from price pi inner join passenger p on p.distance=pi.distance where p.Bus_type="Sitting" and pi.Distance=1000;

Select pi.Bus_Type, pi.price from price pi inner join passenger p on p.Distance = pi.Distance where p.Passenger_name="Pallavi";

SELECT DISTINCT(Distance) FROM  passenger ORDER BY Distance desc;

Select Distance,Passenger_name, Round(Distance * 100.0 / (select sum(Distance) from passenger)) from passenger group by Passenger_name;

create view Query as select Passenger_name,Category from passenger where Category="AC"; 

select * from Query;

DELIMITER //
CREATE PROCEDURE Passenger_list() 
BEGIN
select Passenger_name,Bus_Type from passenger where Bus_Type="Sleeper";
END //
DELIMITER ;
Call Passenger_list;

select * from passenger limit 5;






create database record;
use record;
create table project_data ( project_code varchar(6),project_name varchar(20),project_manager varchar(20),project_budget varchar(20), primary key(project_code));
alter table project_data modify column project_budget int(10);
desc project_data;
insert into project_data values("PC010","Reservation System","Mr.Ajay","120500");
insert into project_data values("PC011","HR Systems","Ms.Charu",500500);
insert into project_data values("PC012","Attendance System","Mr.Rajesh",710700);
select * from project_data;

create table employees(emp_no varchar(4), emp_name varchar(10), dept_no varchar(5),dept_name varchar(10), hourly_rate decimal(4,2),primary key(emp_no));

desc employees;

insert into employees values("S100","Mohan","D03","Database",21.00);
insert into employees values("S101","vipul","D02","Testing",16.50);
insert into employees values("S102","Riyaz","D01","IT",22.00);
insert into employees values("S103","Pavan","D03","Database",18.50);
insert into employees values("S104","Jitendra","D02","Testing",17.00);
insert into employees values("S315","Pooja","D01","IT",23.50);
insert into employees values("S137","Rahul","D03","Database",21.50);
insert into employees values("S218","Avneesh","D02","Testing",15.50);
insert into employees values("S109","Vikas","D01","IT",20.50);

select * from employees;

create table department(dept_no varchar(4),dept_name varchar(10),primary key(dept_no));
 insert into department values("D01","IT");
 insert into department values("D02","Testing");
 insert into department values("D03","Database");
 
 select * from department;
 
 create table employees2(emp_no varchar(4), emp_name varchar(10),hourly_rate decimal(4,2),primary key(emp_no));
 insert into employees2 values("S100","Mohan",21.00);
insert into employees2 values("S101","vipul",16.50);
insert into employees2 values("S102","Riyaz",22.00);
insert into employees2 values("S103","Pavan",18.50);
insert into employees2 values("S104","Jitendra",17.00);
insert into employees2 values("S315","Pooja",23.50);
insert into employees2 values("S137","Rahul",21.50);
insert into employees2 values("S218","Avneesh",15.50);
insert into employees2 values("S109","Vikas",20.50);

select * from employees2;
create table payment (emp_no varchar(4),hourly_rate decimal(4,2));
desc payment;

alter table payment add primary key(emp_no);

insert into payment values("S100",21.00);
insert into payment values("S101",16.50);
insert into payment values("S102",22.00);
insert into payment values("S103",18.50);
insert into payment values("S104",17.00);
insert into payment values("S315",23.50);
insert into payment values("S137",21.50);
insert into payment values("S218",15.50);
insert into payment values("S109",20.50);

